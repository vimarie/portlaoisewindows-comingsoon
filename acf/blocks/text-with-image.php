<?php 

/**
 * ACF Block: Text with image
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$title = get_field('title');
$text = get_field('text');
$image = get_field('image');
$direction = get_field('direction');

?>

<div class="text-with-image">
  <div class="container-fluid">
    <div class="row text-with-image__row <?php if('reverse' == $direction) { echo 'text-with-image__row--reverse'; }?>">
      <div class="col-12 col-md-6">
        <?php if(!empty($title)):?>
          <h2><?php echo apply_filters('the_title', $title);?></h2>
        <?php endif;?>
        <?php echo apply_filters('acf_the_content', $text);?>
        <?php if(!empty($button_text)):?>
          <a href="<?php echo esc_url_raw($button_link);?>" class="button button--text-with-image"><?php echo esc_html();?></a>
        <?php endif;?>
      </div>
      <div class="col-12 col-md-6">
        <div class="text-with-image__picture">
          <?php echo wp_get_attachment_image($image, 'text-with-image', '', ['class' => 'object-fit-cover']);?>
        </div>
      </div>
    </div>
  </div>
</div>