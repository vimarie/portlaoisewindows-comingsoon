<?php

/**
 * ACF Block: Image in grid
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$image = get_field('image');
$image_height = get_field('image_height');

?>

<div class="container-fluid">
  <div class="image-in-grid" style="<?php if(!empty($image_height)) { echo 'height:'.esc_html($image_height).'px'; }?>">
    <?php echo wp_get_attachment_image($image, 'full-width-image', '', ["class" => "object-fit-cover"]); ?>
  </div>
</div>
