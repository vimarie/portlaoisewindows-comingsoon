<?php 

/**
 * ACF Block: Shortcode
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$shortcode = get_field('shortcode');

?>

<div class="container-fluid">
  <?php echo do_shortcode($shortcode);?>
</div>