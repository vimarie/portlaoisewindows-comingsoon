<?php 

/**
 * ACF Block: Block-Text
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$heading = get_field('heading');
$text = get_field('text');

?>

<div class="block-text">
  <div class="heading">
    <?php echo esc_html( $heading ); ?>
  </div>
  <div class="text">
    <?php echo esc_html( $text ); ?>
  </div>
</div>