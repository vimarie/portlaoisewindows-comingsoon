<?php 

/**
 * ACF Block: Section title
 *
 *
 * @package anneslanedental
 * @license GPL-3.0-or-later
 */

$title = get_field('title');
$scalable = get_field('scalable');

?>

<?php if(!empty($title)):?>
  <div class="container-fluid">
    <h2 class="section-title <?php if('true' == $scalable) { echo 'section-title--scalable'; }?>"><?php echo apply_filters('the_title', $title);?></h2>
  </div>
<?php endif;?>