<?php 

/**
 * ACF Block: Logo
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$logo = get_field('logo');

?>

<div class="logo">
  <?php echo wp_get_attachment_image($logo, 'full', '', ['class' => '']); ?>
</div>