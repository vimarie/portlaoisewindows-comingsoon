<?php 

/**
 * ACF Block: Gallery
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$gallery = get_field('gallery');

?>

<?php if(!empty($gallery)):?>
  <div class="gallery">
    <div class="container-fluid">
      <div class="row">
        <?php foreach($gallery as $key => $item):?>
          <div class="col-6 col-lg-4">
            <div class="gallery__image">
              <?php echo wp_get_attachment_image($item['image'], 'gallery', '', ['class' => $item['image_class']]);?>
              <a href="<?php echo wp_get_attachment_image_src($item['image'], 'full', false)[0];?>" data-fancybox="gallery-items" class="cover"></a>
            </div>
          </div>
        <?php endforeach;?>
      </div>
    </div>
  </div>
<?php endif;?>
