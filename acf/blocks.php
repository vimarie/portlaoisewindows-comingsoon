<?php
/**
 * This file configures ACF content blocks.
 *
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

return [

	'homepage-hero' => [
		'title'    => __('Homepage hero', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'blank-title' => [
		'title'    => __('Blank title', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'blank-text' => [
		'title'    => __('Blank text', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],
	
	'full-width-image' => [
		'title'    => __('Full width image', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'post-video' => [
		'title'    => __('Post video', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'iframe' => [
		'title'    => __('Iframe', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'pay-online-form' => [
		'title'    => __('Pay online form', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'thank-you' => [
		'title'    => __('Thank you', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'section-title' => [
		'title'    => __('Section title', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'image-in-grid' => [
		'title'    => __('Image in grid', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'two-images' => [
		'title'    => __('Two images', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'text-with-image' => [
		'title'    => __('Text with image', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'logos' => [
		'title'    => __('Logos', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'centered-text' => [
		'title'    => __('Centered text', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'appointments-form' => [
		'title'    => __('Appointments form', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'vouchers-form' => [
		'title'    => __('Vouchers form', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'gallery' => [
		'title'    => __('Gallery', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'blank-button' => [
		'title'    => __('Blank button', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'logo' => [
		'title'    => __('Logo', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],

	'block-text' => [
		'title'    => __('Block-Text', 'vimarstarter'),
		'category' => 'vimarstarter',
		'align'    => 'full',
	],
];
