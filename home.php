<?php
/**
 * Archive page template
 *
 * @package voltairediamonds
 * @license GPL-3.0-or-later
 */

get_header();

?>

<main class="main main--subpage">
  <?php if (have_posts()) : ?>
    <div class="blog-archive">
      <!-- <h1 class="text-center archive-title"><?php the_title(); ?></h1> -->
      <div class="container-fluid">
        <div class="blog-archive__wrapper">
          <?php while (have_posts()) : ?>
            <?php the_post(); ?>
            <div class="blog-archive__item">
              <div class="blog-archive__image">
                <a href="<?php the_permalink();?>" class="blog-archive__link"></a>
                <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', '', ["class" => "object-fit-cover"]); ?>
              </div>
              <div>
                <a href="<?php the_permalink(); ?>" class="blog-archive__title"><?php the_title(); ?></a>
                <p><time><?php the_time('F j, Y'); ?></time></p>
                <p><?php echo substr(get_the_excerpt(), 0, 300); ?>...</p>
                <a href="<?php the_permalink(); ?>" class="blog-archive__read-more"><?php _e('Read more', 'voltairediamonds'); ?></a>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
        <div class="pagination">
          <?php
            echo paginate_links(array(
              'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
              'current'      => max(1, get_query_var('paged')),
              'format'       => '?paged=%#%',
              'show_all'     => false,
              'type'         => 'list',
              'end_size'     => 2,
              'mid_size'     => 1,
              'prev_next'    => true,
              'prev_text'    => '',
              'next_text'    => '',
              'add_args'     => false,
              'add_fragment' => '',
          ));
          ?>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); ?>
      </div>
    </div>
  <?php endif; ?>
  <div style="height:300px;"></div>

  <?php if (have_posts()) : ?>
    <div class="second-blog">
      <!-- <h1 class="text-center second-blog-title"><?php the_title(); ?></h1> -->
      <div class="container-fluid">
        <div class="second-blog__wrapper">
          <div class="row">
            <div class="col-12 col-lg-9">
              <?php while (have_posts()) : ?>
                <?php the_post(); ?>
                <div class="second-blog__item">
                  <div class="row mt-5">
                    <div class="col-lg-5">
                      <div class="second-blog__image">
                        <a href="<?php the_permalink();?>" class="second-blog__link"></a>
                        <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', '', ["class" => "object-fit-cover"]); ?>
                      </div>
                    </div>
                    <div class="col-lg-7">
                      <div class="second-blog__content">
                        <a href="<?php the_permalink(); ?>" class="second-blog__title"><?php the_title(); ?></a>
                        <p><time><?php the_time('F j, Y'); ?></time></p>
                        <p><?php echo substr(get_the_excerpt(), 0, 300); ?>...</p>
                        <a href="<?php the_permalink(); ?>" class="button button--light second-blog__button"><?php _e('Read more', 'anneslanedental'); ?></a>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
            <div class="col-12 col-lg-3 mt-5">
              <div class="blog-sidebar">
                <h3><?php esc_html_e('Search', 'anneslanedental');?></h3>
                <?php get_sidebar('anneslanedental-sidebar'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="pagination mt-5">
          <?php
            echo paginate_links(array(
              'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
              'current'      => max(1, get_query_var('paged')),
              'format'       => '?paged=%#%',
              'show_all'     => false,
              'type'         => 'list',
              'end_size'     => 2,
              'mid_size'     => 1,
              'prev_next'    => true,
              'prev_text'    => '',
              'next_text'    => '',
              'add_args'     => false,
              'add_fragment' => '',
          ));
          ?>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); ?>
      </div>
    </div>
    <div style="height:300px;"></div>
    <div class="third-blog">
      <div class="container-fluid">
        <div class="third-blog__wrapper">
          <div class="row">
            <?php while (have_posts()) : ?>
              <?php the_post(); ?>
              <div class="col-12 col-md-6 col-lg-4">
                <div class="third-blog__item">
                  <div class="third-blog__image">
                    <a href="<?php the_permalink();?>" class="third-blog__link"></a>
                    <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', '', ["class" => "object-fit-cover"]); ?>
                  </div>
                  <div class="third-blog__content">
                    <div>
                      <a href="<?php the_permalink(); ?>" class="third-blog__title"><?php the_title(); ?></a>
                      <span class="third-blog__time"><time><?php the_time('F j, Y'); ?></time></span>
                      <p><?php echo substr(get_the_excerpt(), 0, 300); ?>...</p>
                    </div>
                      <a href="<?php the_permalink(); ?>" class="button button--light third-blog__button"><?php _e('Read more', 'anneslanedental'); ?></a>
                  </div>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
        <div class="pagination mt-5">
          <?php
            echo paginate_links(array(
              'base'         => str_replace(999999999, '%#%', esc_url(get_pagenum_link(999999999))),
              'current'      => max(1, get_query_var('paged')),
              'format'       => '?paged=%#%',
              'show_all'     => false,
              'type'         => 'list',
              'end_size'     => 2,
              'mid_size'     => 1,
              'prev_next'    => true,
              'prev_text'    => '',
              'next_text'    => '',
              'add_args'     => false,
              'add_fragment' => '',
          ));
          ?>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php wp_reset_query(); ?>
      </div>
    </div>
  <?php endif; ?>

</main>

<?php get_footer(); ?>
