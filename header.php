<?php
/**
 * This file contains theme header elements
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

$global_phone_number = get_field('global_phone_number', 'options');
$global_phone_number_display = get_field('global_phone_number_display', 'options');
$global_logo = get_field('global_logo', 'options');
$global_email = get_field('global_email', 'options');
$global_terms_and_conditions = get_field('global_terms_and_conditions', 'options');
$global_privacy_policy = get_field('global_privacy_policy', 'options');
$global_social_media = get_field('global_social_media', 'options');
$search_shortcode = get_field('search_shortcode', 'options');

$body_classes = get_body_class();

?>

<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
  <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
  <link rel="preconnect" href="https://www.gstatic.com/" crossorigin>
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <?php wp_head(); ?>
</head>

<body <?php body_class('main-body'); ?> >
  <div class="preloader"></div>
  <div id="popup-form"></div>
