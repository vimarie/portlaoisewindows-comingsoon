<?php 

/**
 * This file contains sidebar
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

?>

<div>
  <?php dynamic_sidebar('vimarstarter-sidebar'); ?>
</div>