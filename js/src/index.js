/**
 * External dependencies
 */
import $ from 'jquery';
// import AOS from 'aos';
import 'slick-carousel';
import '@fancyapps/fancybox';
// import 'parallax-js';




// AOS.init();

$(window).on('load', function() {
  // AOS.refresh();
  setTimeout(function(){
    $('.preloader').fadeOut();
  }, 100);

  if($('.subpage-hero').length > 0 && window.location.href.indexOf('sfid') < 0) {
    $('html, body').delay(0).animate({
      scrollTop: $('#subpage-hero-scroll-to').offset().top
    }, 1000);
  }
});

$('p:empty').remove();

import './sections/navigation';
import './sections/single-product';
// import './sections/pay-online';

import './blocks/logos';

import './components/cookies';
import './components/section-title';