import $ from 'jquery';

/* search button */

$('.nav__search-button').on('click', function(e){
  $('.header__search-wrapper').addClass('active');
  $('.header__search-wrapper').slideDown();

  if($('.sf-input-text').length > 0) {
    $('.sf-input-text').focus();
  }
});

/* close button */

$('#header__search-close').on('click', function(){
  $('.header__search-wrapper').slideUp();
  $('.header__search-wrapper').removeClass('active');
});

/* click on body to close */

$(document).mouseup(function(e) {
  var element = $(".header__search-wrapper");

  if (!element.is(e.target) && element.has(e.target).length === 0) {
    element.removeClass('active');
    element.slideUp();
    $('.nav__search-button').removeClass('inactive');
  }
});