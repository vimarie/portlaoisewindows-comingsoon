import $ from 'jquery';

// mobile navigation

$('document').ready(function() {
  $('.hamburger').on('click', function(){
    $(this).toggleClass('active');
    $('.nav__menu').slideToggle();
  });
  
  $('.menu-item-has-children > a').on('click', function(e){
    e.preventDefault();
    if (window.matchMedia('(max-width: 1199px)').matches) {
      $(this).siblings('ul').slideToggle(); 
    }
  });
});
