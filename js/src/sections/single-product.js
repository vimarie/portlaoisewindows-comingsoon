import $ from 'jquery';

$('document').ready(function(){
  if( $('.product-details__photos').length > 0) {
    $('.product-details__photos').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 2,
            dots: true,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            dots: true,
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }
});

// image with details - slick slider and gallery functionality
$(document).ready(function(){
  $(".slick-dots li button").text("");
  $(".slick-next").text("");
  $(".slick-prev").text("");
});

$(window).on('resize', function(){
  $(".slick-prev").text("");
  $(".slick-next").text("");
  $(".slick-dots li button").text("");
});

$('.product-details__photos img').on('click', function(){
  let clickedSRC = $(this).attr("src");
  let currentSRC = $('.product-details__current-image').attr("src");

  $('.product-details__current-image').attr("src", clickedSRC);
  // $('.zoomImg').attr("src", clickedSRC);
});