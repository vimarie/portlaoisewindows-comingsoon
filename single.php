<?php

/**
 * This file contains single post content
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

get_header();
global $post;

$post = get_post();
$page_id = $post->ID;

?>

<main class="main main--subpage">
  <?php if(have_posts()):?>
    <?php while(have_posts()): the_post();?>
      <div class="single-blog-post">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 col-lg-9">
              <div class="single-blog-post__content">
                <div class="single-blog-post__image">
                  <?php echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'full', '', ["class" => "object-fit-cover"]); ?>
                </div>
                <h2><?php the_title();?></h2>
                <p><?php the_content(); ?></p>
              </div>
            </div>
            <div class="col-12 col-lg-3">
              <div class="blog-sidebar">
                <?php get_sidebar('vimarstarter-sidebar'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile;?>
  <?php endif;?>
</main>
<?php get_footer(); ?>
