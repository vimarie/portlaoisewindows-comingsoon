<?php
/**
 * This file contains page elements
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

get_header();
the_post();

$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

$global_logo = get_field('global_logo', 'options');

?>

<main class="main main--subpage <?php if(strpos($url,'privacy-policy') !== false) { echo 'main--privacy-policy'; }?> <?php if(strpos($url,'terms-and-conditions') !== false) { echo 'main--terms-and-conditions'; }?>">
  <?php echo wp_get_attachment_image($global_logo, 'full', '', ['class' => 'mini-logo']);?>
  <?php the_content(); ?>
</main>
<?php get_footer(); ?>
