<?php
/**
 * This file contains front page elements
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

get_header();
the_post();

?>

<main class="main">
  <div class="row">
    <div class="col-md-7 left-sidebar">
      <div class="left-sidebar-container">
        <?php dynamic_sidebar( 'left-sidebar' ); ?>
      </div>
    </div>
    <div class="col-md-5 right-sidebar">
      <div class="right-sidebar-container">
        <?php dynamic_sidebar( 'right-sidebar' ); ?>
      </div>
    </div>
  </div>
</main>
<?php get_footer(); ?>