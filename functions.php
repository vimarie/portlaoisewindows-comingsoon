<?php
/**
 * This file adds functions to the theme.
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */

if (!defined('PAGE_THEME_DIR')) {
  define('PAGE_THEME_DIR', get_theme_root() . '/' . get_template() . '/');
}

if (!defined('PAGE_THEME_URL')) {
  define('PAGE_THEME_URL', WP_CONTENT_URL . '/themes/' . get_template() . '/');
}

require_once(PAGE_THEME_DIR . 'lib/classes/svg-support.php');
require_once(PAGE_THEME_DIR . 'lib/classes/class-tgm-plugin-activation.php');
require_once(PAGE_THEME_DIR . 'lib/functions/required-plugins.php');
require_once(PAGE_THEME_DIR . 'lib/functions/search.php');
require_once(PAGE_THEME_DIR . 'lib/functions/image-object-fit.php');

add_filter('upload_mimes', 'vimarstarter_svg_support');
/**
 * Adds SVG upload support
 *
 * @since 1.0.0
 *
 * @param array $mimes Mime types.
 * @return array
 */
function vimarstarter_svg_support($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

// Enable SVG in admin.
vimarstarter_SVG_Support::enable();

// Add theme supports
add_theme_support('html5');
add_theme_support('align-wide');
add_theme_support('title-tag');
add_theme_support('wp-block-styles');
add_theme_support('post-thumbnails');
add_theme_support('woocommerce');
add_theme_support('wc-product-gallery-lightbox');
add_theme_support('wc-product-gallery-slider');

// Add custom theme image sizes
add_image_size('square', 500, 500, ['center', 'center']);
add_image_size('full-width-image', 1920, 700, ['center', 'center']);
add_image_size('two-images', 700, 700, ['center', 'center']);
add_image_size('bottom-bar-sm', 32, 32, ['center', 'center']);
// Register Navigations
register_nav_menu('Navigation',__('Main navigation'));

//ACF Config
require_once(PAGE_THEME_DIR . 'acf/config.php');

// Remove each style one by one
add_filter('woocommerce_enqueue_styles', function ($enqueue_styles) {
  unset($enqueue_styles[ 'woocommerce-general' ]);
  unset($enqueue_styles[ 'woocommerce-layout' ]);
  return $enqueue_styles;
});

/**
 * Enqeues scripts.
 *
 * @since 1.0.0
 */
add_action('wp_enqueue_scripts', function () {

  wp_dequeue_style('search-filter-plugin-styles');
  wp_deregister_style('search-filter-plugin-styles');

	wp_enqueue_script('vimarstarter-main', get_stylesheet_directory_uri() . '/js/dist/main.js', [ 'jquery' ], filemtime(get_stylesheet_directory() . '/js/dist/main.js'), true);
	wp_enqueue_style('style', get_stylesheet_uri());

});

/**
 * Enqeues block editor assets
 *
 * @since 1.0.0
 */
add_action('enqueue_block_editor_assets', function () {
	wp_enqueue_script('vimarstarter-admin', get_stylesheet_directory_uri() . '/js/dist/editor.js', [ 'wp-data' ], filemtime(get_stylesheet_directory() . '/js/dist/editor.js'), true);
});

/**
 * Enqueue scripts for all admin pages.
 *
 * @since 1.0.0
 */
add_action('admin_enqueue_scripts', function () {
	if (is_admin()) {
		wp_enqueue_style('vimarstarter-admin', get_stylesheet_directory_uri() . '/style-editor.css', [], filemtime(get_stylesheet_directory() . '/style-editor.css'));
	}
});

//Disable WordPress XMLRPC
add_filter('xmlrpc_enabled', '__return_false');

//Unset WordPress Pingbacks
add_filter('wp_headers', function ($headers) {
  unset($headers['X-Pingback']);
  return $headers;
});

// Post rename to Blog in admin sidebar menu
add_action('admin_menu', function () {
  global $menu;
  $menu[5][0] = _x('Blog', 'vimarstarter');
});

add_filter('gform_currencies', function($currencies) {
  $currencies['EUR']['symbol_left'] = '&#8364;';
  $currencies['EUR']['symbol_right'] = '';
  return $currencies;
});

/**
 * Register widget sidebar
 */
function init_widget_sidebar() {

  register_sidebar ([
    'id'            => 'left-sidebar',
    'name'          => __('Left Sidebar', 'vimarstarter'),
    'before_widget' => '<div class="left-sidebar__item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ]);

  register_sidebar ([
    'id'            => 'right-sidebar',
    'name'          => __('Right Sidebar', 'vimarstarter'),
    'before_widget' => '<div class="right-sidebar__item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ]);

}
add_action('widgets_init', 'init_widget_sidebar', 999, 999);