<?php
/**
 * This file contains theme footer bottom
 *
 * @package vimarstarter
 * @license GPL-3.0-or-later
 */
 
$global_phone_number = get_field('global_phone_number', 'options');
$global_phone_number_display = get_field('global_phone_number_display', 'options');
$global_logo = get_field('global_logo', 'options');
$global_email = get_field('global_email', 'options');
$global_terms_and_conditions = get_field('global_terms_and_conditions', 'options');
$global_privacy_policy = get_field('global_privacy_policy', 'options');
$global_social_media = get_field('global_social_media', 'options');

$bottom_bar_social_media = get_field('bottom_bar_social_media', 'options');

$newsletter_shortcode = get_field('newsletter_shortcode', 'options');

$cookies_text = get_field('cookies_text', 'options');

$google_analytics_code = get_field('google_analytics_code', 'options');
$facebook_pixel_code = get_field('facebook_pixel_code', 'options');

?>

<div class="cookies">
	<?php if(!empty($cookies_text)):?>
		<?php echo apply_filters('acf_the_content', $cookies_text);?>
	<?php else:?>
		<p><?php _e('We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you agree with it and you accept our', 'vimarstarter');?>&nbsp;<a href="/privacy-policy"><?php _e('Privacy policy', 'vimarstarter');?></a>.</p>
	<?php endif;?>
  <button class="button cookies__button"><?php _e('I agree', 'vimarstarter');?></button>
</div>

<footer class="footer">

	<div class="bottom-bar">
		<div class="container-fluid">
			<div class="bottom-bar__content">
				<div>
					<p><?php _e('Copyright', 'vimarstarter');?> © <?php echo date("Y"); ?> Portlaoise Windows</p>
					<p><?php _e('All rights reserved', 'vimarstarter');?></p>
					<p><?php _e('Website by', 'vimarstarter');?></p>
					<a href="https://vimar.ie/" target="_blank" class="vimar"><img src="<?php echo get_template_directory_uri();?>/images/vimar.png" alt="vimar-logo"></a>
				</div>
				<div>
          <?php if(!empty($bottom_bar_social_media)):?>
            <ul class="social-media bottom-bar__social-media">
              <?php foreach($bottom_bar_social_media as $key => $item):?>
                <li>
                  <a href="<?php echo esc_url_raw($item['link']);?>" target="_blank">
                    <?php echo wp_get_attachment_image($item['icon'], 'bottom-bar-sm', '', ['class' => 'object-fit-contain']);?>  
                  </a>
                </li>
              <?php endforeach;?>
            </ul>
          <?php endif;?>
          <a href="<?php echo $global_terms_and_conditions;?>"><?php _e('Terms and Conditions', 'vimarstarter');?></a>
          <a href="<?php echo $global_privacy_policy;?>"><?php _e('Privacy Policy', 'vimarstarter');?></a>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php if($google_analytics_code):?>
  <?php echo wp_kses($google_analytics_code, ['script' => ['async' => [], 'src' => []]]);?>
<?php endif; ?>

<?php if($facebook_pixel_code):?>
  <?php echo wp_kses($facebook_pixel_code, ['script' => ['async' => [], 'src' => []]]);?>
<?php endif; ?>

</body>
</html>
<?php wp_footer(); ?>
